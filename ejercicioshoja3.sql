﻿USE ciclistas;

-- 1 ejercicio

SELECT DISTINCT c.edad FROM ciclista c WHERE c.nombre='Banesto';

-- 2 ejercicio

SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo IN ('Banesto','Navigare');

-- 3 ejercicio 

  SELECT DISTINCT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto' AND c.edad BETWEEN 25 AND 32;

-- 4 ejercicio


 SELECT DISTINCT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto' OR c.edad BETWEEN 25 AND 32;

-- 5 ejercicio

SELECT DISTINCT  LEFT(c.nomequipo,1) FROM ciclista c WHERE c.nombre LIKE 'R%';

-- 6 ejercicio
  
SELECT DISTINCT e.numetapa FROM etapa e WHERE e.salida=e.llegada;

-- 7 ejercicio   

 SELECT DISTINCT e.numetapa FROM etapa e WHERE e.salida=e.llegada AND e.dorsal IS NOT NULL;

-- 8 ejercicio

  SELECT DISTINCT p.nompuerto FROM puerto p WHERE p.altura BETWEEN 1000 AND 2000 OR p.altura >2400;

  -- 9 ejercicio

 SELECT DISTINCT p.dorsal FROM puerto p WHERE p.altura BETWEEN 1000 AND 2000 OR p.altura >2400;

-- 10 ejercicio
  
  SELECT  COUNT( DISTINCT e.dorsal)   FROM etapa e;

  -- 11 ejercicio 

SELECT  COUNT( DISTINCT p.numetapa ) FROM puerto p;

-- 12 ejercicio 

  SELECT COUNT(DISTINCT p.dorsal) FROM puerto p;

 -- 13 ejercicio

  SELECT DISTINCT p.numetapa, count(*) FROM puerto p GROUP BY p.numetapa;

  -- 14 ejercicio

   SELECT DISTINCT AVG(p.altura) FROM puerto p;

  -- 15 ejercicio

    SELECT p.numetapa FROM puerto p GROUP BY p.numetapa HAVING AVG (p.altura)>1500;

 -- 16 ejercicio

  SELECT COUNT(*) FROM
  (SELECT p.numetapa FROM puerto p GROUP BY p.numetapa HAVING AVG (p.altura)>1500) c1;

  -- 17 ejercicio

    SELECT l.dorsal,COUNT(*) FROM lleva l GROUP BY l.dorsal;
     
-- 18 ejercicio

SELECT DISTINCT  l.dorsal,codigo, COUNT(*) FROM lleva l GROUP BY l.dorsal,l.código;

-- 19 ejercicio

  SELECT DISTINCT l.dorsal,l.numetapa FROM lleva l GROUP BY l.dorsal,l.numetapa; 






   



  
   


    
     

     







  
   




  








  


